//
//  YCTDevice.h
//  BTList
//
//  Created by Ruslan Dejarov on 01/05/16.
//  Copyright © 2016 youct.ru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCTDevice : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *detail;

- (instancetype)initWithName:(NSString*)name detail:(NSString*)detail;

@end
