//
//  YCTDevice.m
//  BTList
//
//  Created by Ruslan Dejarov on 01/05/16.
//  Copyright © 2016 youct.ru. All rights reserved.
//

#import "YCTDevice.h"

@implementation YCTDevice

- (instancetype)initWithName:(NSString*)name detail:(NSString*)detail
{
    self = [super init];
    if(self) {
        self.name = name;
        self.detail = detail;
    }
    
    return self;
}

@end
