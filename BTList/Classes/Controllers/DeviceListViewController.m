//
//  DeviceListViewController.m
//  BTList
//
//  Created by Ruslan Dejarov on 01/05/16.
//  Copyright © 2016 youct.ru. All rights reserved.
//

#define PULL_TEXT_COLOR [UIColor whiteColor]
#define PULL_BACKGROUND_COLOR [UIColor colorWithRed:0.251 green:0.663 blue:0.827 alpha:1]

#define YCT_DEVICE_LIST_CELL @"kYCT_DEVICE_LIST_CELL"

#define MAX_TIMEOUT_TO_WAIT_DISCOVERING 10.0f

#import "DeviceListViewController.h"
#import "LGRefreshView.h"
#import "YCTDevice.h"
#import "YCTDeviceService.h"

@interface DeviceListViewController ()
@property (strong, nonatomic) LGRefreshView *refreshView;
@property (strong, nonatomic) NSArray *devices;
@property (strong, nonatomic) YCTDeviceService *service;
@end

@implementation DeviceListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addPullToRefresh];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.devices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:YCT_DEVICE_LIST_CELL];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:YCT_DEVICE_LIST_CELL];
    }
    
    YCTDevice *device = self.devices[indexPath.row];
    
    cell.textLabel.text = device.name;
    cell.detailTextLabel.text = device.detail;
    
    return cell;
}

- (void)addPullToRefresh
{
    __weak typeof(self) wself = self;
    self.refreshView = [LGRefreshView refreshViewWithScrollView:self.tableView refreshHandler:^(LGRefreshView *refreshView) {
        [YCTDeviceService discoverDevicesWithSuccess:^(NSArray *devices) {
            [wself.refreshView endRefreshing];
            
            wself.devices = devices;
            [wself.tableView reloadData];
        }];
        
        //max waiting for discovering
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MAX_TIMEOUT_TO_WAIT_DISCOVERING * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
            [self.refreshView endRefreshing];
        });
    }];
    
    self.refreshView.tintColor = PULL_TEXT_COLOR;
    self.refreshView.backgroundColor = PULL_BACKGROUND_COLOR;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
