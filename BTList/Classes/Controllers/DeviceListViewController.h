//
//  DeviceListViewController.h
//  BTList
//
//  Created by Ruslan Dejarov on 01/05/16.
//  Copyright © 2016 youct.ru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceListViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@end
