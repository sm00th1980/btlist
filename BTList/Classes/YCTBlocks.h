//  Blocks.h
//  powerman
//
//  Created by Ruslan Dejarov on 27/12/15.
//  Copyright (c) 2015 Gembird Europe BV. All rights reserved.
//

typedef void (^void_block_array)(NSArray *);
typedef void (^void_block_void)(void);
