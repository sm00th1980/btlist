//
//  YCTBLEService.h
//  BTList
//
//  Created by Ruslan Dejarov on 01/05/16.
//  Copyright © 2016 youct.ru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YCTBlocks.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface YCTDeviceService : NSObject <CBCentralManagerDelegate>

+ (void)discoverDevicesWithSuccess:(void_block_array)success;

@end
