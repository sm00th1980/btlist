//
//  YCTBLEService.m
//  BTList
//
//  Created by Ruslan Dejarov on 01/05/16.
//  Copyright © 2016 youct.ru. All rights reserved.
//

#import "YCTDeviceService.h"
#import "YCTDevice.h"

@interface YCTDeviceService ()
@property (strong, nonatomic) CBCentralManager *centralManager;
@property (copy, nonatomic) void_block_array success;
@end

@implementation YCTDeviceService

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (void)discoverDevicesWithSuccess:(void_block_array)success
{
    [YCTDeviceService sharedInstance].success = success;
    
    [[YCTDeviceService sharedInstance] setup];
    
}

- (void)setup
{
    [YCTDeviceService sharedInstance].centralManager = [[CBCentralManager alloc] initWithDelegate:self
                                                                                            queue:dispatch_get_main_queue()]; //use main thread
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    // You should test all scenarios
    if (central.state != CBCentralManagerStatePoweredOn) {
        return;
    }
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        // Scan for devices
        [_centralManager scanForPeripheralsWithServices:nil options:nil];
        
        NSLog(@"Scanning started");
    }
}


- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (_success) {
        YCTDevice *device = [[YCTDevice alloc] initWithName:peripheral.name detail:peripheral.identifier.UUIDString];
        
        _success(@[device]);
    }
}

@end
